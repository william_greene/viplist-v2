<?php

/** 
 *  Chirp Hackathon - vipli.st - @newtman, @jeremiahlee, @gregarious, @jhstrauss, @dustinwhittle, @basictheory
 *
 *   - http://chirphackday.pbworks.com/VIPList
 *   - http://github.com/klout/VIPList
 *
 *   Plancast - http://noopsi.com/entry/989/
 *   Klout - http://developer.klout.com
 *   awe.sm - http://developers.awe.sm
**/

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once(dirname(__FILE__).'/../lib/YahooYQLQuery.class.php');

$plancast_url = isset($_GET['plancast_url']) ? parse_url($_GET['plancast_url']) : false;
if(empty($plancast_url))
{
  die(json_encode(array('error' => 'A plancast url is required to provide awesm klout analytics.')));
}

$info = explode('/', $plancast_url['path']);
$plancast_attendance_id = $info[count($info) - 1];

$yql = new YahooYQLQuery();
$plancast_rsp = $yql->execute("use 'http://www.vipli.st/yql/plancast.plans.xml' as plancast.plans; select * from plancast.plans where attendance_id='".$plancast_attendance_id."'");
$plancast = $plancast_rsp->query->results->json;
$pl_attendees = $plancast->attendees;

$attendee_list = array();

foreach($pl_attendees as $pl_attendee) {
  $kl_url = "http://api.klout.com/1/users/show.json";
  $rsp = YahooCurl::fetch($kl_url, array(
    'key' => 'y8j4bta3ezvk6texdedjzewq',
    'users' => $pl_attendee->screen_name,
    'priority' => 'chirp'
  ));
  
  $klout = json_decode(html_entity_decode($rsp['response_body']));
  
  if($klout->status != '404') {
    $klout_user = $klout->users[0]->user;
    
    $attendee = new stdclass();
    $attendee->id = $pl_attendee->id;
    $attendee->image = $pl_attendee->profile_image_url;
    $attendee->name = $pl_attendee->name;
    $attendee->klout = $klout_user->score->kscore;
    
    // $attendee->time
    // $attendee->parent_awesm = TBD
    
    $awesm_url = "http://create.awe.sm/url.xml";
    $rsp = YahooCurl::fetch($awesm_url, array(
       'version' => '1',
       'api_key' => '4e69002f0073c015caf97b561c60e42791dc395e93092d0c979d0b2c04468d9d',
       'details' => 'true',
       'original_url' => $plancast_url
    ));
    $awesm_info = json_decode($rsp['response_body']);
    print_r($awesm_info);
    
    $attendee->parent_id = "";
    
    $attendee_list[] = $attendee;
  }
}

header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 

$response = array('plancast' => $plancast, 'attendees' => $attendee_list);

print json_encode($response);
