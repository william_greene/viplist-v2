#!/bin/bash
cd /var/www
TODAY=`date +%Y-%m-%d-%H-%M-%S`
DEPLOYDIR="/var/www/deploy/viplist-$TODAY"
cp -r /home/ubuntu/VIPList $DEPLOYDIR
rm current-viplist
ln -s $DEPLOYDIR current-viplist