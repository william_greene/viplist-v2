<?php

abstract class Visualization {

	protected $creator;
	protected $creatorId;
	protected $attendees;
	protected $map;
	protected $rootNodes;
	protected $extra;

	public function __construct($creatorId,$attendees,$map,$rootNodes,$extra=null)
	{
		$this->creatorId = $creatorId;
		$this->creator = $attendees[$creatorId];
		$this->attendees = $attendees;
		$this->map = $map;
		$this->rootNodes = $rootNodes;
		$this->extra = $extra;
		//error_log("Map created as " . print_r($this->map,true));
	}

	public abstract function draw();

}
