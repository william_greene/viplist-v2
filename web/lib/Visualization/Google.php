<?php

class Visualization_Google extends Visualization {

	public function draw()
	{
		error_log(print_r($this->map, true));

		$vertices = array();
		foreach ($this->map as $node)
		{
			$vertices[] = $node['parent_id'] . '--' . $node['child_id'];
		}
		$graphString = implode(';', $vertices);
		error_log("Graph as string: $graphString");

		$chartImg = 'https://chart.googleapis.com/chart?' . http_build_query(array(
								'cht' => 'gv',
								'chl' => 'graph{' . $graphString . '}',
										//'chs' => '750x400'
						));

		echo '<img src="' . $chartImg . '">';
	}

}
