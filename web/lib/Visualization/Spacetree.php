<?php

class Visualization_SpaceTree extends Visualization {

	/*
	 * foreach node in list
	 *		if the node is a child of the parent, add as branch
	 *			branch = getTree(the child,allNodes)
	 */
	public function getTree($parentId,$nodeList,$loopDetection=null)
	{
		if(is_null($loopDetection)) $loopDetection = array();
		$tree = array(
				'id' => $parentId,
				'name' => $parentId,
				'data' => new stdClass(),
				'children' => array()
		);
		//error_log("Starting tree for $parentId");
		foreach($nodeList as $node) {
			$nodeParent = $node['parent_id'];
			$nodeChild = $node['child_id'];
			if ($nodeParent == $parentId && $nodeParent != $nodeChild && !in_array($nodeChild,$loopDetection)) { // ignore loops
				//error_log("Parent of $nodeChild is $nodeParent, creating a subtree");
				$loopDetection[] = $nodeParent;
				$tree['children'][] = $this->getTree($nodeChild,$nodeList,$loopDetection);
			}
		}
		return $tree;
	}

	public function draw()
	{
		// create tree from root node
		$tree = $this->getTree($this->creatorId,$this->map);
		// use tree to calculate totals per attendee
		$this->setAttendeeTreeTotals($tree);
		//error_log("Attendees: " . print_r($this->attendees,true) );
		// draw tree
		$this->drawTree($tree);
	}

	// recursively sum up totals per attendee and insert them into the attendees
	private function setAttendeeTreeTotals($tree)
	{
		//error_log("Tree: " . print_r($tree,true));
		$totalForTree = count($tree['children']);
		foreach($tree['children'] as $branch)
		{
			$totalForTree += $this->setAttendeeTreeTotals($branch);
		}
		$attendeeId = $tree['id'];
		//error_log("Attendee $attendeeId " . print_r($this->attendees[$attendeeId],true) );
		$this->attendees[$attendeeId]['sub_attendees']['cumulative'] = $totalForTree;
		//error_log("Attendee $attendeeId cumulative $totalForTree");
		return $totalForTree;
	}

	private function drawTree($tree,$divId='infovis')
	{
		?>
<script type="text/javascript" src="/js/jit-yc.js"></script>
<script type="text/javascript">
	/*
	 * awe.sm/Plancast invitation tree mapping visualization
	 * Copyright (c) 2011 Snowball Factory, Inc. except where noted.
	 * Uses excellent SpaceTree library from JIT (thejit.org)
	 */

	var labelType, useGradients, nativeTextSupport, animate;

	(function() {
		var ua = navigator.userAgent,
		iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
		typeOfCanvas = typeof HTMLCanvasElement,
		nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
		textSupport = nativeCanvasSupport
			&& (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
		//I'm setting this based on the fact that ExCanvas provides text support for IE
		//and that as of today iPhone/iPad current text support is lame
		labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
		nativeTextSupport = labelType == 'Native';
		useGradients = nativeCanvasSupport;
		animate = !(iStuff || !nativeCanvasSupport);
	})();

	/*
	 * JavaScript Pretty Date
	 * Originally copyright (c) 2008 John Resig (jquery.com)
	 * Extended to handle dates > 1 month ago, and fixed bug at 1 week.
	 * Licensed under MIT license
	 */
	// Takes an ISO time and returns a string representing how
	// long ago the date represents.
	function prettyDate(time){
		var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
			diff = (((new Date()).getTime() - date.getTime()) / 1000),
			day_diff = Math.floor(diff / 86400);

		if ( isNaN(day_diff) || day_diff < 0 )
			return;

		return day_diff == 0 && (
				diff < 60 && "just now" ||
				diff < 120 && "1 minute ago" ||
				diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
				diff < 7200 && "1 hour ago" ||
				diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
			day_diff == 1 && "Yesterday" ||
			day_diff <= 7 && day_diff + " days ago" ||
			day_diff <= 60 && Math.ceil( day_diff / 7 ) + " weeks ago" ||
			"more than 2 months ago";
	}

	var attendees = <?php echo json_encode($this->attendees); ?>;
	var plan = <?php echo json_encode($this->extra); ?>;

	function init<?php echo $divId; ?>(){

		//init data
		var json = <?php echo json_encode($tree); ?>;

		//init Spacetree
		//Create a new ST instance
		var st = new $jit.ST({
				//id of viz container element
				injectInto: '<?php echo $divId; ?>',
				//set duration for the animation
				duration: 300,
				//set animation transition type
				transition: $jit.Trans.Quart.easeInOut,
				//set distance between node and its children
				levelDistance: 50,
				// show full tree from the start?
				constrained: true,
				// how deep can tree be?
				levelsToShow: 1,
				//enable panning
				Navigation: {
					enable:true,
					panning:true
				},
				// nodes go top-down or left-right
				//orientation: 'left',
				orientation: 'top',
				// alignment
				alignment: 'right',
				// how far from the center should the node be
				offsetY: 0,
				offsetX: 0,
				//set node and edge styles
				//set overridable=true for styling individual
				//nodes or edges
				Node: {
					height: 80,
					width: 120,
					type: 'rectangle',
					color: 'transparent',
					overridable: true
				},

				Edge: {
					type: 'bezier',
					overridable: true
				},

				onBeforeCompute: function(node){
					//Log.write("loading " + node.name);
				},

				onAfterCompute: function(){
					//Log.write("done");
				},

				//This method is called on DOM label creation.
				//Use this method to add event handlers and styles to
				//your node.
				onCreateLabel: function(label, node){
					label.id = node.id;
					//label.innerHTML = node.name;
					label.innerHTML = makeNode(node);
					label.onclick = function(){
						st.onClick(node.id, { Move: {
								enable: true,
								offsetX: st.canvas.translateOffsetX,
								offsetY: st.canvas.translateOffsetY
						} });
					};
					label.childNodes.item(0).childNodes.item(0).onmouseover = function() {
						showToolTip(node,label);
					}

				},

				//This method is called right before plotting
				//a node. It's useful for changing an individual node
				//style properties before plotting it.
				//The data properties prefixed with a dollar
				//sign will override the global node style properties.
				onBeforePlotNode: function(node){
					//add some color to the nodes in the path between the
					//root node and the selected node.
					//node.onclick = function() { alert('hi'); };
					if (node.selected) {
						//node.data.$color = "#ff7";
					}
					else {
						delete node.data.$color;
						//if the node belongs to the last plotted level
						if(!node.anySubnode("exist")) {
							//count children number
							var count = 0;
							node.eachSubnode(function(n) { count++; });
							//assign a node color based on
							//how many children it has
							//node.data.$color = ['#aaa', '#baa', '#caa', '#daa', '#eaa', '#faa'][count];
						}
					}
				},

				//This method is called right before plotting
				//an edge. It's useful for changing an individual edge
				//style properties before plotting it.
				//Edge data proprties prefixed with a dollar sign will
				//override the Edge global style properties.
				onBeforePlotLine: function(adj){
					//adj.data.$color = "#F0F0F0"; // #ff8000 original orange
					//adj.data.$color = "#ff8000";
					adj.data.$color = "#ddd";
					adj.data.$lineWidth = 2;

					var childNodeId = adj.nodeTo.id;
					if (typeof attendees[childNodeId]['inviting_channel'] != 'undefined') {
						var invitingChannel = attendees[childNodeId]['inviting_channel'];
						if (invitingChannel == 'twitter') {
							adj.data.$lineWidth = 3;
							//adj.data.$color = "#94e4e8"; // or 32ccff
							adj.data.$color = "#32ccff";
						} else if (invitingChannel == 'facebook') {
							adj.data.$lineWidth = 3;
							adj.data.$color = "#3b5997";
						} else if (invitingChannel == 'googlebuzz') {
							adj.data.$lineWidth = 3;
							adj.data.$color = "#008f37";
						}
					} else if (adj.nodeFrom.id == plan.attendee.id) { // child of root
						//adj.data.$color = "#ff8000";
						//adj.data.$color = "#F0F0F0";
						adj.data.$color = "#ddd";
					}
				},

				onAfterCompute: function() {
					//st.switchPosition("top","replot");
				}
		});
		//load json data
		st.loadJSON(json);
		//compute node positions and layout
		st.compute();
		//optional: make a translation of the tree
		//st.geom.translate(new $jit.Complex(-200, 0), "current");
		//emulate a click on the root node.
		st.onClick(st.root);
		//end
	}

	var rootNodeId = plan['attendee']['id'];

	var makeNode = function(node) {
		var id = node.id;
		var childrenCode = '';
		var withChildrenClass = '';
		if (typeof attendees[id]['sub_attendees'] !== 'undefined' && attendees[id]['sub_attendees']['cumulative'] > 0 && id != rootNodeId) {
			childrenCode = '<div class="children">'
			childrenCode += '+' + attendees[id]['sub_attendees']['cumulative'];
			childrenCode += '</div>';
			withChildrenClass = 'withChildren';
		}
		var html = '<div class="attendee">' +
					'<div class="squarePic channel-' + attendees[id]['inviting_channel'] + '">' +
						'<div class="avatar" style="background-image: url(\'' + attendees[id]['pic_square'] + '\');"></div>' +
					'</div>' +
					'<div class="user ' + withChildrenClass + '">' +
						'<div class="username">' + attendees[id]['best_name'] + '</div>' +
						childrenCode +
					'</div></div>';
		return html;
	}

	var previousLabel = null;
	var previousInner = null;
	var previousNode = null;
	var toolTipVisible = false;
	var showToolTip = function(node,label) {

		var id = node.id;
		var attendee = attendees[id];

		hideToolTip(previousNode,previousLabel); // hide previous tooltip if any, if necessary

		// store for reset
		previousLabel = label;
		previousInner = label.innerHTML;
		previousNode = node;
		var code =
			'<div class="infotip">'+
			label.childNodes.item(0).innerHTML;
		if(node.id == '__others') {
			code += '<div class="via others">These users signed up without being invited by a friend.</div>';
		} else {
			// creator didn't join, creator created
			if (node.id == rootNodeId) {
				joinVerb = 'Created this event '
			} else {
				joinVerb = 'Joined';
			}

			// how long ago joined/created
			code += '<div class="joined">' + joinVerb + ' ' + prettyDate(attendee['timestamp']) + '</div>';

			// how many people invited
			if (attendee['sub_attendees']['cumulative'] && node.id != rootNodeId) {
				code +='<div class="via total">' + attendee['sub_attendees']['total'] + ' direct, ' + attendee['sub_attendees']['cumulative'] + ' total</div>';
			}

			// invitees per channel, with clicks
			if (attendee['clicks']['twitter']) {
				code +='<div class="via"><img src="/images/twitter.png"><span class="attendcount"> ' + attendee['clicks']['twitter'] + ' clicks <br>' + attendee['sub_attendees']['twitter'] + ' attending</span></div>';
			}
			if (attendee['clicks']['facebook']) {
				code +='<div class="via"><img src="/images/facebook.png"><span class="attendcount"> ' + attendee['clicks']['facebook'] + ' clicks <br> ' + attendee['sub_attendees']['facebook'] + ' attending</span></div>';
			}
			if (attendee['clicks']['googlebuzz']) {
				code +='<div class="via"><img src="/images/buzz.png"><span class="attendcount"> ' + attendee['clicks']['googlebuzz'] + ' clicks <br>' + attendee['sub_attendees']['googlebuzz'] + ' attending</span></div>';
			}
		}
		code += '</div>';
		label.innerHTML = code;
		// turn username into a link
		var usernameLink = label.childNodes.item(0).childNodes.item(1).childNodes.item(0);
		if(usernameLink && node.id != '__others') {
			usernameLink.innerHTML = '<a target="_blank" href="http://plancast.com/user/' + id + '">' + attendees[id]['best_name'] + '</a>';
		}
		// hide tooltip when we mouseout of dark rectangle
		label.childNodes.item(0).onmouseout = function(e) {
			if (e.toElement.tagName == 'A') return; // it thinks the link is mousing out for some reason.
			if (e.currentTarget != e.target) return; // its definition of "out" is weird
			hideToolTip(node,label);
		};
		// if we mouse out of bubble, hide if we're moving to the canvas, but otherwise keep it showing
		label.onmouseout = function(e) {
		   if (e.toElement.tagName == 'CANVAS') {
			   hideToolTip(node,label);
		   }
			
		}
		toolTipVisible = true;
	}
	// hides a tool tip if there is one
	var hideToolTip = function(node,label) {
		if(toolTipVisible) {
			// reset to the previous HTML
			label.innerHTML = previousInner;
			// reapply the previous mouseover listener
			label.childNodes.item(0).childNodes.item(0).onmouseover = function() {
				showToolTip(node,label);
			}
			toolTipVisible = false;
		}
	}

</script>

<div id="<?php echo $divId; ?>"></div>
<script>window.addEventListener('load',init<?php echo $divId; ?>,false);</script>

		<?php
	}

}