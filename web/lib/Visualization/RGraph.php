<?php

class Visualization_RGraph extends Visualization {

	/*
	 * foreach node in list
	 *		if the node is a child of the parent, add as branch
	 *			branch = getTree(the child,allNodes)
	 */
	public function getTree($parentId,$nodeList,$loopDetection=null)
	{
		if(is_null($loopDetection)) $loopDetection = array();
		$tree = array(
				'id' => $parentId,
				'name' => $parentId,
				'data' => new stdClass(),
				'children' => array()
		);
		//error_log("Starting tree for $parentId");
		foreach($nodeList as $node) {
			$nodeParent = $node['parent_id'];
			$nodeChild = $node['child_id'];
			if ($nodeParent == $parentId && $nodeParent != $nodeChild && !in_array($nodeChild,$loopDetection)) { // ignore loops
				//error_log("Parent of $nodeChild is $nodeParent, creating a subtree");
				$loopDetection[] = $nodeParent;
				$tree['children'][] = $this->getTree($nodeChild,$nodeList,$loopDetection);
			}
		}
		return $tree;
	}

	public function draw()
	{

		if (ALL_ROOT_OF_CREATOR) {
			// create tree from root node
			$tree = $this->getTree($this->creatorId,$this->map);
			$this->drawTree($tree);
		}
		else
		{
			foreach($this->rootNodes as $rootNode)
			{
				$this->drawTree($this->getTree($rootNode,$this->map),'tree'.$rootNode);
			}
		}
	}

	private function drawTree($tree,$divId='infovis')
	{

		?>
<script type="text/javascript" src="/js/jit.js"></script>
<script type="text/javascript">
var labelType, useGradients, nativeTextSupport, animate;

(function() {
	var ua = navigator.userAgent,
	iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
	typeOfCanvas = typeof HTMLCanvasElement,
	nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
	textSupport = nativeCanvasSupport
		&& (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
	//I'm setting this based on the fact that ExCanvas provides text support for IE
	//and that as of today iPhone/iPad current text support is lame
	labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
	nativeTextSupport = labelType == 'Native';
	useGradients = nativeCanvasSupport;
	animate = !(iStuff || !nativeCanvasSupport);
})();

var init = function() {
	var json = <?php echo json_encode($tree); ?>;
	var rgraph = new $jit.RGraph({
    //Where to append the visualization
    injectInto: 'infovis',
    //Optional: create a background canvas that plots
    //concentric circles.
    background: {
      CanvasStyles: {
        strokeStyle: '#f4f4f4'
      }
    },
    //Add navigation capabilities:
    //zooming by scrolling and panning.
    Navigation: {
      enable: true,
      panning: true,
      zooming: 10
    },
    //Set Node and Edge styles.
    Node: {
        color: '#ddeeff'
    },

    Edge: {
      color: '#C17878',
      lineWidth:1.5
    },

    onBeforeCompute: function(node){
        //Log.write("centering " + node.name + "...");
        //Add the relation list in the right column.
        //This list is taken from the data property of each JSON node.
        //$jit.id('inner-details').innerHTML = node.data.relation;
    },

    onAfterCompute: function(){
        //Log.write("done");
    },
    //Add the name of the node in the correponding label
    //and a click handler to move the graph.
    //This method is called once, on label creation.
    onCreateLabel: function(domElement, node){
        domElement.innerHTML = node.name;
        domElement.onclick = function(){
            rgraph.onClick(node.id);
        };
    },
    //Change some label dom properties.
    //This method is called each time a label is plotted.
    onPlaceLabel: function(domElement, node){
        var style = domElement.style;
        style.display = '';
        style.cursor = 'pointer';

        if (node._depth <= 1) {
            style.fontSize = "1.4em";
            style.color = "#000";

        } else if(node._depth == 2){
            style.fontSize = "1.0em";
            style.color = "#494949";

        } else {
            style.fontSize = "0.8em";
            style.color = "#494949";
        }

        var left = parseInt(style.left);
        var w = domElement.offsetWidth;
        style.left = (left - w / 2) + 'px';
    }
	});
	//load JSON data
	rgraph.loadJSON(json);
	//trigger small animation
	rgraph.graph.eachNode(function(n) {
		var pos = n.getPos();
		pos.setc(-200, -200);
	});
	rgraph.compute('end');
	rgraph.fx.animate({
		modes:['polar'],
		duration: 2000
	});
}
</script>
<div id="infovis" style="background-color:#fff; width: 800px; height: 800px;"></div>
<script>
	window.addEventListener('load',init);
</script>
		<?php

	}
}