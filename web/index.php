<?php
require('controller.inc');
?><!doctype html>
<!--[if lt IE 7 ]><html class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>VIPLi.st by awe.sm | <?= $plan['what'] ?></title>
	<meta name="description" content="A visual map of how attendees discovered and signed up for <?= $plan['what'] ?>, <?= $plan['when'] ?> at <?= $plan['where'] ?>.">
	<meta property="og:title" content="VIPLi.st by awe.sm | <?= $plan['what'] ?>">
	<meta property="og:type" content="article">
	<meta property="og:url" content="http://www.vipli.st/?url=<?= urlencode($url) ?>">
	<meta property="og:site_name" content="VIPLi.st">
	<meta property="fb:admins" content="2904704,500815044">
	<meta name="viewport" content="width=1024, user-scalable=no">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/<?php echo $vizMethod; ?>.css">
<?php if (!empty($url)) { ?>
	<link rel="canonical" href="http://www.vipli.st/?url=<?= urlencode($url) ?>">
<?php } ?>
	<script src="/js/libs/modernizr-1.6.min.js"></script>
	<script>
		var touchMove = function(event) {
			// Prevent scrolling on this element
			event.preventDefault();
		}
	</script>
</head>

<body lang="en">
	<div id="container" ontouchmove="touchMove(event);">
		<header class="clearfix">
			<div id="prompt">
				<h1><abbr title="Very Important Person">VIP</abbr>Li.st</h1>
				<form method="get">
					<label id="plancastUrlLabel" for="plancastUrl">Enter a Plancast event to track its virality with awe.sm.</label>
					<input id="plancastUrl" name="url" type="text" placeholder="http://" value="<?php echo $url; ?>">
				</form>
				<p class="popular"><strong>Popular Plans:</strong> <?php
				$topPlanLinks = array();
				foreach($topThreePlans as $topPlan)
				{
					$topPlanLinks[] = '<a href="/?url=' . urlencode($topPlan['plan_url']) . '">' . neat_trim($topPlan['what'],15) . '</a>';
				}
				echo implode(' • ',$topPlanLinks);
				?></p>
			</div>

			<div id="eventSummary" class="clearfix">
				<div id="eventSummaryHead">
					<h2><?= $plan['what'] ?></h2>
					<p><?= $plan['where'] ?> • <?= $plan['when'] ?></p>
				</div>
				<div id="eventSummaryDesc">
					<p><?= neat_trim($plan['description'],140) ?> <a href="<?= $staticPlanLink ?>" target="_blank" title="<?= $plan['what'] ?>">Count me in!</a></p>
				</div>
			</div>
		</header>

		<div id="main" role="main">
			<?php
				if (!empty($map))
				{
					$vizClass = "Visualization_$vizMethod";
					$viz = new $vizClass($creatorId,$attendees,$map,null,$plan);
					$viz->draw();
				}
			?>
		</div>

		<div id="stats">
			<table>
				<tr>
					<td rowspan="3" class="totalStat"><strong><?= $plan['attendees_count'] ?></strong><br>Attending</td>
					<td class="channelStat"><?= $plan['attendees_by_channel']['twitter'] ?></td>
					<td>&nbsp;<em>via</em> Twitter</td>
				</tr>	
				<tr>
					<td class="channelStat"><?= $plan['attendees_by_channel']['googlebuzz'] ?></td>
					<td>&nbsp;<em>via</em> Google Buzz</td>
				</tr>
				<tr>
					<td class="channelStat"><?= $plan['attendees_by_channel']['facebook'] ?></td>
					<td>&nbsp;<em>via</em> Facebook</td>
				</tr>
			</table>
			
			<table>
				<tr>
					<td rowspan="3" class="totalStat"><strong><?= $plan['shares_by_channel']['total'] ?></strong><br>Shares</td>
					<td class="channelStat"><?= $plan['shares_by_channel']['twitter'] ?></td>
					<td>&nbsp;<em>via</em> Twitter</td>
				</tr>	
				<tr>
					<td class="channelStat"><?= $plan['shares_by_channel']['googlebuzz'] ?></td>
					<td>&nbsp;<em>via</em> Google Buzz</td>
				</tr>
				<tr>
					<td class="channelStat"><?= $plan['shares_by_channel']['facebook'] ?></td>
					<td>&nbsp;<em>via</em> Facebook</td>
				</tr>
			</table>
			
			
			<table>
				<tr>
					<td rowspan="3" class="totalStat"><strong><?= $plan['clicks_by_channel']['total'] ?></strong><br>Clicks</td>
					<td class="channelStat"><?= $plan['clicks_by_channel']['twitter'] ?></td>
					<td>&nbsp;<em>via</em> Twitter</td>
				</tr>	
				<tr>
					<td class="channelStat"><?= $plan['clicks_by_channel']['googlebuzz'] ?></td>
					<td>&nbsp;<em>via</em> Google Buzz</td>
				</tr>
				<tr>
					<td class="channelStat"><?= $plan['clicks_by_channel']['facebook'] ?></td>
					<td>&nbsp;<em>via</em> Facebook</td>
				</tr>
			</table>
		</div>
		
		<div id="about" style="display:none">
			<p class="bottom">Powered by <a href="http://totally.awe.sm/">awe.sm</a>, <em>VIPLi.st</em> uses data from <a href="http://plancast.com/">Plancast</a> to create a visual map of how attendees discovered and signed up for an event. These trees illustrate the spread of information through communities across multiple channels, and tie results back to the share actions that drove them.</p>
			<p><a href="http://totally.awe.sm/">awe.sm</a> brings understanding to social media marketing. We demonstrate value and provide insight to measure success. <a href="http://totally.awe.sm/">Learn more&hellip;</a></p>
		</div>

		<footer>
			<a id="btnFullScreen" href="#fullscreen"><span class="footerSprite"></span>Full Screen</a>
			<a id="btnAbout" href="#about"><span class="footerSprite"></span>About</a>
			
			<div id="shareTools">
				<script src="http://tools.awe.sm/tweet-button/files/widgets.js" type="text/javascript"></script>
				<div><a href="http://twitter.com/share" class="twitter-share-button"
						awesm:key="d701d90a56c367f752e88bd72d66f86fadabca629d06e9b7ace529f9bd9cd2d1"
						data-via="awesm"
						<?php
						if (empty($url)) {
							?>data-text="Check out VIPList, a visual map of how @plancast events spread on Twitter and Facebook: "<?php
						} else {
							?>data-text="Check out this cool map of the attendees for <?= $plan['what'] ?>: "<?php
						}
						?>
						data-related="plancast"
						data-count="horizontal">Tweet</a></div>
				<awesm:fblike show_faces="false" layout="button_count" width="100" awesm:key="d701d90a56c367f752e88bd72d66f86fadabca629d06e9b7ace529f9bd9cd2d1"></awesm:fblike>
				<script src="http://widgets.awe.sm/v1/widgets.js"></script>
			</div>
		</footer>
	</div>

	<script type="text/javascript">
	var uservoiceOptions = {
	  /* required */
	  key: 'viplist',
	  host: 'viplist.uservoice.com',
	  forum: '102209',
	  showTab: true,
	  /* optional */
	  alignment: 'right',
	  background_color:'#f00',
	  text_color: 'white',
	  hover_color: '#06C',
	  lang: 'en'
	};

	function _loadUserVoice() {
	  var s = document.createElement('script');
	  s.setAttribute('type', 'text/javascript');
	  s.setAttribute('src', ("https:" == document.location.protocol ? "https://" : "http://") + "cdn.uservoice.com/javascripts/widgets/tab.js");
	  document.getElementsByTagName('head')[0].appendChild(s);
	}
	_loadSuper = window.onload;
	window.onload = (typeof window.onload != 'function') ? _loadUserVoice : function() { _loadSuper(); _loadUserVoice(); };
	</script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
	<script>!window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.4.4.min.js"%3E%3C/script%3E'))</script>
	<script>
		var footerMode = "stats";
		var displayMode = "normal";
		
		function toggleFullScreen() {
			if (displayMode == "normal") {
				$("#eventSummary").hide();
				$("#prompt").hide();
				$("#btnAbout").hide();
				$("#shareTools").hide();
				
				if (footerMode == "stats") {
					$("#stats").hide();
				} else {
					$("#about").hide();
				}
				
				displayMode = "full";
				document.getElementById("btnFullScreen").className = "active";
				$("footer").addClass("active");
			} else {
				$("#eventSummary").show();
				$("#btnAbout").show();
				$("#prompt").show();
				$("#shareTools").show();
				
				if (footerMode == "stats") {
					$("#stats").show();
				} else {
					$("#about").show();
				}
				
				displayMode = "normal";
				document.getElementById("btnFullScreen").className = "";
				$("footer").removeClass("active");
			}
		}
		$("#btnFullScreen").click(toggleFullScreen);
		
		function toggleInfo() {
			if (footerMode == "stats") {
				$("#about").show();
				$("#stats").hide();
				document.getElementById("btnAbout").className = "active";
				footerMode = "info";
			} else {
				$("#stats").show();
				$("#about").hide();
				document.getElementById("btnAbout").className = "";
				footerMode = "stats";
			}
		}
		$("#btnAbout").click(toggleInfo);
		
		var _gaq=[['_setAccount','UA-21102483-1'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
</body>
</html>