<?php

require('lib/util.php');
require('lib/awesm.class.php');
require('lib/Visualization.php');
require('lib/Visualization/Google.php');
require('lib/Visualization/Graphviz.php');
require('lib/Visualization/Spacetree.php');
require('lib/Visualization/Flare.php');
require('lib/Visualization/RGraph.php');

$awesm = new Awesm('d1e2e1a15f46fc92af741e0e9c0f787d9f62cf2a242b91d684057fe8d3f2a230');
$cache = new Memcache();
$cache->connect('localhost','11211');

$url = $_GET['url'];
$mode = @$_GET['mode'];
$vizMethod = @$_GET['viz'];
if (empty($vizMethod)) $vizMethod = 'SpaceTree';
$doCache = false;

// fetch and cache "popular plancasts": top 10 original urls, by clicks
$eightDaysAgo = strftime('%Y-%m-%d',time() - 691200);
$oneDayAgo = strftime('%Y-%m-%d',time() - 86400);
$topClicksData = json_decode($cache->get('topclicks'),true);
if (!$topClicks)
{
	$topClicksData = $awesm->getClickStats($eightDaysAgo, $oneDayAgo, null, null, 'original_url', 1, 10, null, null, null, true);
	$cache->set('topclicks'.$planId,json_encode($topClicks),null,3600); // 1 hour
}
// sort them by clicked shares
$topClicks = $topClicksData['groups'];
$shareCounts = array();
foreach($topClicks as $index => $share) {
	$shared = $share['clicked_shares'];
	$shareCounts[$index] = $shared;
}
arsort($shareCounts);
$topFour = array();
// pick the three most-shared out of the top clicked urls
for($i = 0; $i < 4; $i++)
{
	$key = key($shareCounts);
	$topFour[] = $topClicks[$key];
	next($shareCounts);
}
// set default url at random from one of the most popular
$randomPopular = rand(0,3);
if(empty($url)) $url = $topFour[$randomPopular]['original_url'];
unset($topFour[$randomPopular]); // remove the one we're showing from the list
// for the remaining three, get plan details
$topThreePlans = array();
foreach($topFour as $planClicks)
{
	$urlParts = explode('/', $planClicks['original_url']);
	$topPlanId = $urlParts[count($urlParts) - 1];
	$topThreePlans[] = getPlan($topPlanId);
}
error_log(print_r($topThreePlans,true));

// now get the plan we're actually rendering
if (!empty($url) || $mode = 'demo')
{

	$urlParts = explode('/', $url);
	$planId = $urlParts[count($urlParts) - 1];

	if ($mode == 'demo')
	{
		$planId = $_GET['planid'];
		if(empty($planId)) $planId = '3r1e';
		$url = 'http://plancast.com/p/'.$planId;

		// get data from files
		$response = file_get_contents("data/test_plan_$planId.json");
		$plan = json_decode($response, true);

		$response = file_get_contents("data/test_stats_$planId.json");
		$stats = json_decode($response, true);

		$response = file_get_contents("data/test_shares_$planId.json");
		$sharesData = json_decode($response, true);

	} else
	{
		$plan = json_decode($cache->get('plan_'.$planId),true);
		if (!$plan)
		{
			// plan data
			$plan = getPlan($planId,true);
			$cache->set('plan_'.$planId,json_encode($plan),null,300);
		}
		$planUrl = $plan['attendance_url'];

		$stats = json_decode($cache->get('stats_'.$planId),true);
		if (!$stats)
		{
			// click stats
			$stats = $awesm->getClickStats(null, null, array('original_url' => $planUrl), null, 'awesm_url', 1, 500, null, null, null, true);
			$cache->set('stats_'.$planId,json_encode($stats),null,300);
		}

		$sharesData = json_decode($cache->get('sharesdata_'.$planId),true);
		if (!$sharesData)
		{
			// shares
			$shares = $awesm->getShareStats(null, null, array('original_url' => $planUrl), '.json', null, null, 'awesm_url');

			// shares metadata via clicks-list with metadata (huge hack)
			$shareList = array();
			foreach($shares['groups'] as $shareCount) {
				$shareList[] = $shareCount['awesm_url'];
			}
			$sharesData = $awesm->getClicksList($shareList, null, null, null, false, null, 500, null, null, true);
			$cache->set('sharesdata_'.$planId,json_encode($sharesData),null,300);
		}
	}

	// get a "count me in" link for this plan
	$staticPlanLink = $awesm->createStaticLink($plan['plan_url'],array(
		'notes' => 61115,
		'tool' => '3y2wTw',
	));
	error_log("Static link is " . $staticPlanLink);

	// used in several places
	$creator = $plan['attendee'];
	$creatorId = $creator['id'];

	//error_log(print_r($stats,true));
	// extract all the attendees and index them by parent awesm: these are "children"
	$attendeesParents = array();
	$attendees = array();
	foreach ($plan['attendees'] as $attendee)
	{
		$awesmParent = $attendee['awesm'];
		$userId = $attendee['id'];
		if (!empty($awesmParent))
		{
			$attendeesParents[$userId] = $awesmParent;
		}
		// index all users for lookups later
		$attendees[$userId] = $attendee;
	}

	//error_log("Plans with parent data: " . print_r($attendeesParents,true));

	// look at all the links and map them to the user who created them. These are "parents"
	$urlsToUsers = array();
	$urlsMap = array();
	foreach ($stats['groups'] as $awesmUrlData)
	{
		$awesmUrl = $awesmUrlData['metadata']['awesm_id'];
		$userId = $awesmUrlData['metadata']['notes'];
		$urlsToUsers[$awesmUrl] = $userId;
		$urlsMap[$awesmUrl] = $awesmUrlData;
	}
	// fetch shares that have no clicks from the shares data and include them
	foreach($sharesData['share_list'] as $share)
	{
		$awesmUrl = $share['awesm_url'];
		$userId = $share['metadata']['notes'];
		if (!array_key_exists($awesmUrl,$urlsToUsers)) {
			$attendees[$userId]['no_clicks_yet'] = true;
		}
	}

	// fix formatting of users
	foreach($attendees as $i => &$attendee) {
		$attendee['best_name'] = $attendee['username'];
		if (empty($attendee['username'])) {
			$attendee['best_name'] = $attendee['name'];
		}
		$attendee['clicks'] = array(
			'total_clicks' => 0
		);
		$attendee['sub_attendees'] = array(
			'twitter' => 0,
			'facebook' => 0,
			'googlebuzz' => 0,
			'total' => 0
		);
		//if (empty($attendee['username'])) unset($attendees[$i]);
	}

	//error_log("urls created by users: " . print_r($urlsToUsers, true));

	// add clicks and shares to each user from the links they own, and the global
	$plan['clicks_by_channel'] = array(
		'twitter' => 0,
		'facebook' => 0,
		'googlebuzz' => 0,
		'total' => 0
	);
	$plan['shares_by_channel'] = array(
		'twitter' => 0,
		'facebook' => 0,
		'googlebuzz' => 0,
		'total' => 0
	);
	foreach($urlsToUsers as $awesmUrl => $userId)
	{
		$clicks = $urlsMap[$awesmUrl]['clicks'];
		$channel = $urlsMap[$awesmUrl]['metadata']['channel'];
		if($channel == 'facebook-share' || $channel == 'facebook-post') $channel = 'facebook';

		$attendees[$userId]['clicks']['total_clicks'] += $clicks;
		$attendees[$userId]['clicks'][$channel] = $clicks;
		
		$plan['shares_by_channel']['total']++;
		$plan['shares_by_channel'][$channel]++;
		$plan['clicks_by_channel'][$channel] += $clicks;
		$plan['clicks_by_channel']['total'] += $clicks;
	}

	// create the parent-child map
	$map = array();
	$allChildren = array(); // tracking list used later
	$plan['attendees_by_channel'] = array(
		'twitter' => 0,
		'facebook' => 0,
		'googlebuzz' => 0,
		'total' => 0
	);
	foreach ($attendeesParents as $childId => $parentAwesm)
	{
		$parentId = $urlsToUsers[$parentAwesm];
		$map[] = array(
				'parent_id' => $parentId,
				'child_id' => $childId
		);
		$allChildren[] = $childId;

		// add this childid to the count of attendees by channel of share
		$channel = $urlsMap[$parentAwesm]['metadata']['channel'];
		if($channel == 'facebook-share' || $channel == 'facebook-post') $channel = 'facebook';
		$plan['attendees_by_channel']['total']++;
		$plan['attendees_by_channel'][$channel]++;

		$attendees[$childId]['inviting_channel'] = $channel;

		// add this attendee count to the parent's count
		$attendees[$parentId]['sub_attendees']['total']++;
		$attendees[$parentId]['sub_attendees'][$channel]++;
	}

	// mark root nodes as all being children of the original creator
	//error_log("all children: " . print_r($allChildren,true));
	$rootLinks = array();
	foreach ($map as $node)
	{
		$parentId = $node['parent_id'];
		if (!in_array($parentId, $allChildren))
		{
			//error_log("$parentId is not a child of anybody");
			$rootLinks[$parentId] = array(
					'parent_id' => $creatorId,
					'child_id' => $parentId,
			);
			$allChildren[] = $parentId;
		}
	}
	$map = array_merge($map, $rootLinks);

	// add all other attendees not already listed
	// if they've shared, list them individually
	// if not, add them as a special child of root
	$noClickSharers = array();
	$extraPeople = array(
		'id' => '__others',
		'username' => 'do not use',
		'pic_square' => '/images/crowd.png',
		'clicks' => array( 'total_clicks' => 0),
		'others_list' => array()
	);
	foreach($attendees as $attendee)
	{
		$attendeeId = $attendee['id'];
		//error_log($attendee['username']);
		//error_log("User: " . print_r($attendee,true));
		if (array_key_exists('no_clicks_yet',$attendee))
		{
			//error_log("No clicks yet for " . $attendee['username'] . " so adding $attendeeId to $creatorId");
			$map[] = array(
				'parent_id' => $creatorId,
				'child_id' => $attendeeId
			);
		}
		else if (!in_array($attendeeId,$urlsToUsers))
		{
			$extraPeople['clicks']['total_clicks']++;
			$extraPeople['others_list'][] = $attendee;
		}
	}
	if ($extraPeople['clicks']['total_clicks'] > 0)
	{
		$extraPeople['best_name'] = 'and ' . $extraPeople['clicks']['total_clicks'] . ' others';
		$attendees['__others'] = $extraPeople;
		$map[] = array(
					'parent_id' => $creatorId,
					'child_id' => '__others'
				);
	}
	//error_log("Map: " . print_r($map,true));

	// cleanup the map looking for empty attendee nodes
	foreach($map as $nodeId => $node) {
		if (!array_key_exists('username',$attendees[$node['parent_id']]))
		{
			//error_log("Parent id for {$node['child_id']} not found in attendees");
			unset($map[$nodeId]);
		}
		if (!array_key_exists('username',$attendees[$node['child_id']]))
		{
			//error_log("Attendee info for {$node['child_id']} not found in attendees");
			unset($map[$nodeId]);
		}
	}

	// strip attendees from the plan as this is duplicated
	unset($plan['attendees']);
}

//error_log(print_r($plan, true));
//error_log(print_r($attendees, true));
